#!/usr/bin/env python3

import gitlab
import json
import os
import argparse
import datetime
import yaml
import collections
import shutil

# creating a list of paths in the CI yml file, so that I can find a specific one
# not looking for values of "script" and "rules" part
def make_path_list(json, pathlist = [], parent = ""):
    exclusion_list = ["script", "rules", "before_script", "after_script", "services", "parallel", "cache", "include", "dependencies"]
    if isinstance(json, dict):
        for key in json.keys():
            path = key
            if parent:
                path = parent + ":" + key
            if path not in pathlist:
                pathlist.append(path)
            if key not in exclusion_list:
                make_path_list(json[key], pathlist, path)
    else:
        if isinstance(json, list):
            for value in json:
                if isinstance(value, list) or isinstance(value, dict):
                    for array_value in value:
                        path = parent + ":" + array_value
                        pathlist.append(path)
                else:
                    path = f"{parent}:{value}"
                    pathlist.append(path)
        else:
            path = parent + ":" + str(json)
            pathlist.append(path)
    return pathlist

def get_merged_yml(gl_project):
    try:
        lint_result = gl_project.ci_lint.get(include_merged_yaml=True)
        if lint_result.valid == True:
            return lint_result.merged_yaml
        else:
            print("YML invalid")
            return None
    except Exception as err:
        print("Could not lint CI file:")
        print(err)
        return None

def get_group_projects(gl, groups):
    projects = []
    for topgroup in groups:
        group = gl.groups.get(topgroup)
        group_projects = group.projects.list(iterator=True, include_subgroups=True)
        for group_project in group_projects:
            projects.append(group_project.attributes["id"])
    return projects

def check_project_adoption(project, ci_yml_pathlist, adoption_criteria):
    adoption = {}
    for adoption_key in adoption_criteria:
        adoption_criterium = adoption_criteria[adoption_key]
        found = False
        for ci_keyword_path in ci_yml_pathlist:
            # here we check if the path of CI keywords we want to be adopted is in the CI file
            if adoption_criterium["path"] in ci_keyword_path:
                found = True
                break
        adoption[adoption_key] = int(found)
    return adoption

def sort_by_group(project_adoptions):
    # sort groups alphabetically, sort projects in groups alphabetically, produce a list
    sorted_projects = {}
    for project in project_adoptions:
        project_group = project["path"]
        project_group = project_group[0:project_group.rfind("/")]
        project["group"] = project_group
        if project_group in sorted_projects:
            sorted_projects[project_group].append(project)
            sorted_projects[project_group] = sorted(sorted_projects[project_group], key=lambda s: s["name"])
        else:
            sorted_projects[project_group] = [project]
    sorted_projects = collections.OrderedDict(sorted(sorted_projects.items()))
    project_list = []
    for group in sorted_projects:
        projects = sorted_projects[group]
        for project in projects:
            project_list.append(project)
    return project_list

def get_group_count(project_adoptions):
    groups = set()
    for project in project_adoptions:
        groups.add(project["group"])
    return len(groups)

def get_adoption_report(gl, projects, adoption_criteria, date, args):
    project_adoptions = []
    pathlists = []
    for project_id in projects:
        print("[Info] Checking adoption of project %s" % project_id)
        project = gl.projects.get(project_id)
        has_ci = False

        if project.repository_access_level == "disabled" or project.empty_repo:
            print("[Info] Project %s has no repo, skipping" % project_id)
            continue

        if "ci_config_path" in project.attributes:
            ci_config_path = project.attributes["ci_config_path"] if project.attributes["ci_config_path"] else '.gitlab-ci.yml'
            ci_file = None
            try:
                print("[Info] Getting %s for project %s" % (ci_config_path, project_id))
                ci_file = project.files.get(file_path=ci_config_path, ref=project.default_branch)
                has_ci = True
            except Exception as e:
                print("[ERROR] Could not get CI file '%s' for project %s" % (ci_config_path, project.attributes["path_with_namespace"]))
                print(e)
            parsed_ci = {}
            if ci_file is not None:
                try:
                    merged_yml = get_merged_yml(project)
                    parsed_ci = yaml.safe_load(merged_yml)
                except Exception as e:
                    print("[ERROR] Could not get merged yaml for project %s" % (project.attributes["path_with_namespace"]))
                    has_ci = False
                    print(e)
            pathlist = []
            if parsed_ci:
                try:
                    pathlist = make_path_list(parsed_ci, pathlist)
                except Exception as e:
                    print("[ERROR] Could not parse keys for project %s" % (project.attributes["path_with_namespace"]))
                    has_ci = False
                    print(e)
        else:
            print("[ERROR] ci_config_path not in project attributes. Make sure your token can read_repo.")
            exit()
            pathlist = []
        project_adoption = {}
        project_adoption["name"] = project.name
        project_adoption["path"] = project.attributes["path_with_namespace"]
        project_adoption["url"] = project.attributes["web_url"]
        project_adoption["has_ci"] = int(has_ci)
        project_adoption["date"] = date
        project_adoption.update(check_project_adoption(project, pathlist, adoption_criteria))
        project_adoptions.append(project_adoption)
        project_paths = {}
        project_paths["name"] = project.name
        project_paths["path"] = project.attributes["path_with_namespace"]
        project_paths["url"] = project.attributes["web_url"]
        project_paths["paths"] = pathlist
        pathlists.append(project_paths)
    return project_adoptions, pathlists

# we want to make sure that all criteria are set for all projects, even for previous crawls
# initialize all criteria for existing data to 0
# migrate previously used "a_" prefix for criteria to verbatim usage of criteria keys (without a_)
def align_criteria(existing_data, adoption_criteria):
    aligned_data = []
    for data in existing_data:
        for criterium in adoption_criteria:
            if criterium not in data:
                if "a_" + criterium not in data:
                    print("Missing %s in %s, setting to 0." % (criterium, data))
                    data[criterium] = 0
                else:
                    data[criterium] = data["a_" + criterium]
                    del data["a_" + criterium]
        aligned_data.append(data)
    return aligned_data

# only keep 10 data points
def limit_data_points(project_adoptions):
    data_points = set([data["date"] for data in project_adoptions])
    data_points = sorted(data_points)
    limited_data = []
    if len(data_points) > 10:
        # find middle data point
        # delete all data with that date
        middle = data_points[5]
        print("[Info] Found more than 10 data points. Deleting %s to keep data size reasonable." % middle)
        for data in project_adoptions:
            if data["date"] != middle:
                limited_data.append(data)
    else:
        return project_adoptions
    return limited_data

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='YML file that defines requested groups, projects and adoption parameters')
parser.add_argument('--gitlab', help='URL of the gitlab instance', default="https://gitlab.com/")

args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token, retry_transient_errors=True)
date = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

configfile = args.configfile
groups = []
projects = []
adoption_criteria = {}

with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    if "groups" in config:
        groups = config["groups"]
        projects = get_group_projects(gl, groups)
    else:
        if not "projects" in config:
            print("Error: No group or project configured. Stopping.")
            exit(1)
        else:
            projects = config["projects"]

    if "adoption_criteria" in config:
        adoption_criteria = config["adoption_criteria"]
    else:
        print("No adoption_criteria provided, exiting")

project_adoptions = get_adoption_report(gl, projects, adoption_criteria, date, args)
pathlist = project_adoptions[1]
project_adoptions = sort_by_group(project_adoptions[0])

# make sure we have the data folder
if not os.path.exists('data'):
    os.mkdir('data')

if not os.path.isfile("data/adoption_data.json"):
    with open("data/adoption_data.json", "w") as reportfile:
        json.dump(project_adoptions, reportfile, indent = 2)
else:
    existing_data = []

    # after adding the new crawl, pull previous data and merge to common data file
    # merge so that for each project, there will be a list of adoption entries, each with the appropriate date
    with open("data/adoption_data.json", "r") as reportfile:
        existing_data = json.load(reportfile)
        existing_data = align_criteria(existing_data, adoption_criteria)
    project_adoptions.extend(existing_data)
    project_adoptions = limit_data_points(project_adoptions)
    with open("data/adoption_data.json", "w") as reportfile:
        json.dump(project_adoptions, reportfile, indent = 2)

shutil.copyfile("data/adoption_data.json", "public/adoption_data.json")

with open("public/adoption_criteria.json", "w") as adoptionfile:
    json.dump(adoption_criteria, adoptionfile, indent = 2)

with open("public/ci_paths.json", "w") as pathfile:
    json.dump(pathlist, pathfile, indent = 2)

