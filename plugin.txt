git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Enable deoplete by adding let g:deoplete#enable_at_startup = 1 to your .vimrc.


" Enable filetype plugins
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Vundle itself
Plugin 'VundleVim/Vundle.vim'

" Plugins for Python development
Plugin 'davidhalter/jedi-vim'
Plugin 'nvie/vim-flake8'
Plugin 'scrooloose/nerdtree'
Plugin 'majutsushi/tagbar'
Plugin 'tpope/vim-virtualenv'
Plugin 'dense-analysis/ale'
Plugin 'Valloric/YouCompleteMe'
Plugin 'airblade/vim-gitgutter'

" All of your Plugins must be added before the following line
call vundle#end()            " Required
filetype plugin indent on    " Required
syntax on                    " Turn on syntax highlighting

" Set encoding to UTF-8
set encoding=utf-8

" Use spaces instead of tabs
set expandtab
" Be smart when using tabs ;)
set smarttab
" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4
" Enable auto-indent
set autoindent
" Show line numbers
set number
" Highlight the current line
set cursorline

" Set for Python development
" Use <F8> to toggle the Tagbar
nmap <F8> :TagbarToggle<CR>
" Use <F7> to run flake8 and show the linting errors
map <F7> :Flake8<CR>
" Set the Python path for jedi-vim
let g:jedi#auto_vim_configuration = 0
" Activate a virtualenv
let g:virtualenv_directory= 'path/to/virtualenvs'
" Settings for ale
let g:ale_linters = {
\   'python': ['flake8', 'mypy'],
\}
" Settings for YouCompleteMe
let g:ycm_auto_trigger = 1
